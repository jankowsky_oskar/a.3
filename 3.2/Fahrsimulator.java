import java.util.Scanner;

public class Fahrsimulator {

    public static void main(String[] args) {

        Fahrsimulator sim = new Fahrsimulator();
        Scanner sc = new Scanner(System.in);

        System.out.println("'0' to stop program.");

        while (true) {

            System.out.println("\nAktuelle Geschwindigkeit: " + sim.getV() + " km/h.");
            System.out.print("Um wie viel soll das Auto beschleunigen: ");

            int dv = sc.nextInt();

            if (dv == 0)
                break;

            sim.beschleunige(dv);
        }
        sc.close();

    }

    private double v;

    public Fahrsimulator() {
        v = 0;
    }

    void beschleunige(double dv) {

        double newV = this.v + dv;

        if (newV < 0) {
            System.out.println("Mindestgeschwindigkeit unterschritten.");
        } else if (newV > 130) {
            System.out.println("Maximalgeschwindigkeit überschritten.");
        } else {
            this.v = newV;
        }
    }

    public double getV() {
        return v;
    }
}
