import java.util.Scanner;

public class PCHaendler {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString("was m�chten Sie bestellen?", sc);

		int anzahl = liesInt("Geben Sie die Anzahl ein:", sc);

		double preis = liesDouble("Geben Sie den Nettopreis ein:", sc);

		System.out.println();
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:", sc);

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtnettopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

	public static String liesString(String text, Scanner sc) {
		System.out.println(text);
		return sc.nextLine();
	}

	public static int liesInt(String text, Scanner sc) {
		System.out.println(text);
		return sc.nextInt();
	}

	public static double liesDouble(String text, Scanner sc) {
		System.out.println(text);
		return sc.nextDouble();
	}

	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		return anzahl * nettopreis;
	}

	public static double berechneGesamtnettopreis(double nettogesamtpreis, double mwst) {
		return nettogesamtpreis * (1 + mwst / 100);
	}

	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,
			double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");

	}

}