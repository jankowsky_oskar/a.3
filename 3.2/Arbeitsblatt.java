public class Arbeitsblatt {

    public static void main(String[] args) {
    }

    // Aufgabe 3
    static double reihenschaltung(double r1, double r2) {
        return r1 + r2;
    }

    // Aufgabe 4
    static double parallelschaltung(double r1, double r2) {
        return 1 / r1 + 1 / r2;
    }

    // Aufgabe 5
    class Mathe {

        // Aufgabe 7
        double hypotenuse(double k1, double k2) {
            return Math.sqrt(quadrieren(k1) + quadrieren(k2));
        }

        double quadrieren(double x) {
            return x * x;
        }

    }

}
