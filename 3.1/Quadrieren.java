public class Quadrieren {

	public static void main(String[] args) {

		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		double x = 5;

		double ergebnis = square(x);

		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}

	static double square(double x) {
		return x * x;
	}
}
